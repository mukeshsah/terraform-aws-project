resource "aws_instance" "instance1" {
  ami                    = "ami-053b0d53c279acc90"
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.securitygroup.id]
  subnet_id              = aws_subnet.subnet1.id
  user_data              = base64encode(file("userdata1.sh"))
}

resource "aws_instance" "instance2" {
  ami                    = "ami-053b0d53c279acc90"
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.securitygroup.id]
  subnet_id              = aws_subnet.subnet2.id
  user_data              = base64encode(file("userdata2.sh"))
}