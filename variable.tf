variable "access_key" {
  type    = string
  default = "Your Access Key"
}

variable "secret_key" {
  type    = string
  default = "Your Secret Key"
}


